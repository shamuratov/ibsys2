from django.urls import path
from .import views
from ..sellwish_directsales import views as view_sell

urlpatterns = [
    path('capacity-requirements', views.index, name='cap_home'),
    path('export_to_xml', views.export_to_xml, name='export_to_xml'),
    path('update_capacity_requirement', views.update_capacity_requirement, name='update_capacity_requirement'),
    path('reset_application', view_sell.reset_application, name='reset_application')
]
