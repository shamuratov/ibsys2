from django.apps import AppConfig


class CapacityRequirementsConfig(AppConfig):
    name = 'capacity_requirements'
