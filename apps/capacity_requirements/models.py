from django.db import models


class Workingplace(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    wp_id = models.PositiveIntegerField()
    shift = models.PositiveIntegerField(default=1)
    overtime = models.FloatField(default=0)
    previous_overtime = models.FloatField(default=0)
    production_time_new = models.FloatField(default=0)
    setup_time_new = models.FloatField(default=0)
    total_time_needed = models.FloatField(default=0)

    def __str__(self):
        return "%s %s %s %s" % (
          self.wp_id,
          self.shift,
          self.overtime,
          self.previous_overtime
        )


def get_article_quantity_tuples_from_workingplaces():
    """
    returns contents of each row of Workingplace table in form of tuple
    in this order: (stations, shifts, overtime)
    :return: f.e. [(1, 1, 0),...]
    """
    _temp = list(Workingplace.objects.values_list("wp_id", "shift", "overtime"))
    return [(x[0], x[1], int(float(x[2]))) for x in _temp]  # convert "0.00" -> 0

