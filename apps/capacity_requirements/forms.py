from django import forms


class UploadFileForm(forms.Form):
    export_xml = forms.FilePathField()