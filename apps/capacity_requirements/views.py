from apps.capacity_requirements.lib.calculations import get_capacity_list_for_all_workplaces
from apps.capacity_requirements.lib.constants import _WORKINGPLACES
from django.forms import modelformset_factory, TextInput
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect

from apps.production_orders.models import substract_direct_sales_from_lager
from .models import Workingplace

Formset = modelformset_factory(
    Workingplace,
    fields=(
        'id', 'wp_id', 'shift', 'overtime', 'previous_overtime', 'production_time_new', 'setup_time_new',
        'total_time_needed'),
    extra=0, max_num=15,
    widgets={"id": TextInput(attrs={'readonly': True, 'style': 'width: 20px'}),
             "wp_id": TextInput(attrs={'readonly': True, 'style': 'width: 20px'}),
             "total_time_needed": TextInput(attrs={'readonly': True}),
             "previous_overtime": TextInput(attrs={'readonly': True}),
             "overtime": TextInput(attrs={'readonly': True})})

UPDATE_PRESSED: bool = False


def index(request):
    """
    Capacity_Requirement-Objects from the DB will be rendered in HTML as a Form
    :param request:
    :return:
    """
    template = 'capacity_requirements/index.html'
    formset = Formset()
    global UPDATE_PRESSED
    if not UPDATE_PRESSED:
        update_total_time_needed()  # after update - this step should be skipped
    recalculate_table_results()
    context = {'formset': formset}
    UPDATE_PRESSED = False
    return render(request, template, context)


def update_total_time_needed():
    # calculates time needed to produce production-orders
    total_production_time: list = get_capacity_list_for_all_workplaces()

    for wp, amnt in zip(_WORKINGPLACES, total_production_time):
        Workingplace.objects.filter(wp_id=wp).update(production_time_new=amnt)


def recalculate_table_results():
    db_state: list = list(Workingplace.objects.all().values())
    for row in db_state:
        row['total_time_needed'] = row['production_time_new'] + row['setup_time_new'] + row['previous_overtime']
        # calculates shifts and overtime
        if 0 <= row['total_time_needed'] <= 3600:
            row['shift'] = 1
            row['overtime'] = 0
            if 2400 < row['total_time_needed'] <= 3600:
                row['overtime'] = (row['total_time_needed'] - 2400) / 5

        elif 3600 < row['total_time_needed'] <= 6000:
            row['shift'] = 2
            row['overtime'] = 0
            if 4800 < row['total_time_needed'] <= 6000:
                row['overtime'] = (row['total_time_needed'] - 2400) / 5

        elif 6000 < row['total_time_needed'] <= 7200:
            row['shift'] = 3
            row['overtime'] = 0
        else:
            row['shift'] = 3

        # saves data back to db
        Workingplace(**row).save()


def export_to_xml(request):
    """
    :param request:
    :return:
    """
    from utils.make_xml_for_export import combine_all_xml_parts
    substract_direct_sales_from_lager()
    file = combine_all_xml_parts()
    response = HttpResponse(file, content_type='application/xml')
    response['Content-Disposition'] = 'attachment; filename=inputdaten.xml'
    return response


def update_capacity_requirement(request):
    if request.method == 'POST':
        formset = Formset(request.POST)
        if formset.is_valid():
            form_cap = formset.save(commit=False)
            for f in form_cap:
                f.save()
            recalculate_table_results()
    global UPDATE_PRESSED
    UPDATE_PRESSED = True
    return HttpResponseRedirect('capacity-requirements')
