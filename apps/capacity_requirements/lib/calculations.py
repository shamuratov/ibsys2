from apps.capacity_requirements.lib.constants import _PRODUCTION_COEFFICIENTS, _WORKINGPLACES
from apps.production_orders.models import get_dict_item_quantity_from_productorders


def get_dict_ownpart_and_production_time(workplace_id: int) -> dict:
    """
    for the given workplace_id will be produced dictionary with pair {"ownpart_id": production_time}
    :param workplace_id: f.e. 1
    :return: {"E49": 6, "E54": 6, "E29": 6}
    """
    for dict_pair in _PRODUCTION_COEFFICIENTS:
        for k in dict_pair.keys():
            if k == workplace_id:
                return dict_pair.get(k)


def calculate_total_production_time_on_workplace(workplace_id: int) -> int:
    """
    returns amount of minutes needed to produce ProductOrders for a given dictionary with pairs , workplace_id
    F.e. for ('E49', 100, 1) = 600
    :param workplace_id: 1
    :return:
    """
    from collections import OrderedDict
    total_capacity: int = 0
    _wp = OrderedDict()
    _wp = get_dict_ownpart_and_production_time(workplace_id)
    dict_productorder_amount: dict = get_dict_item_quantity_from_productorders()

    for ownpart_id, production_time in _wp.items():
        for own_id, production_amount in dict_productorder_amount.items():
            if own_id in ("P1", "P2", "P3"):
                if own_id == ownpart_id:
                    total_capacity += production_amount * production_time
            else:
                if own_id == ownpart_id:
                    total_capacity += production_amount * production_time
    return total_capacity


def get_capacity_list_for_all_workplaces() -> list:
    """
    creates a list with new capacity for each workingplace
    corresponds to "Kapazitaetsbedarf (neu)"
    :return: list with all values for total production time for all workingplaces
    """
    new_capacity = list()
    for wp in _WORKINGPLACES:
        new_capacity.append(calculate_total_production_time_on_workplace(wp))
    return new_capacity
