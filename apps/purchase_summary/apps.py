from django.apps import AppConfig


class PurchaseSummaryConfig(AppConfig):
    name = 'purchase_summary'
