from django.urls import path
from . import views
from ..sellwish_directsales import views as view_sell

urlpatterns = [
    path('production-summary', views.index, name='production-summary'),
    path('update_safety_stock', views.update_safety_stock, name='update_safety_stock'),
    path('reset_application', view_sell.reset_application, name='reset_application')
]
