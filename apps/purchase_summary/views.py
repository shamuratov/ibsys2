from django.shortcuts import render
from django.forms import modelformset_factory
from django.http import HttpResponseRedirect
from apps.sellwish_directsales.views import get_purchase_summary_data
from apps.sellwish_directsales.models import ProductionProgram
from apps.production_orders.models import Ownparts
from utils.products.Product1 import Product1
from utils.products.Product2 import Product2
from utils.products.Product3 import Product3

Formset_Ownparts = modelformset_factory(
    Ownparts,
    fields=('item_name', 'safety_stock',),
    extra=0)

AMOUNT_TO_PRODUCE = []


def index(request):
    """
    Product_Order-Objects from the DB
    will be rendered in HTML as a Form
    """

    product_1 = get_purchase_summary_data(1)
    product_2 = get_purchase_summary_data(2)
    product_3 = get_purchase_summary_data(3)

    template = 'purchase_summary/index.html'

    p1 = Product1()
    sw_p1 = ProductionProgram.objects.get(product_id="P1")
    p1_ownparts = Ownparts.objects.get(item_name="P1")
    amount_to_produce_p1 = (sw_p1.quantity_1 + p1_ownparts.safety_stock) - p1_ownparts.quantity
    if amount_to_produce_p1 <= 0:
        e_parts_needed_for_sellwish_p1 = 0
    else:
        e_parts_needed_for_sellwish_p1 = p1.get_e_part_count(count=amount_to_produce_p1)
        if sw_p1.quantity_1 > 0:
            for p in product_1:
                if str(p["parts_number"]) != "P1":
                    p["sw_needed"] = e_parts_needed_for_sellwish_p1[p["parts_number"]]
                    if len(p1.get_e_part_count_for_e_part(p["parts_number"], count=1).keys()) > 0:
                        atp = (p["sw_needed"] + p["safety_stock"]) - (
                                p["current_stock"] + p["orders_in_queue"] + p["orders_in_progress"])
                        for v in p1.get_e_part_count_for_e_part(p["parts_number"], count=1).keys():
                            if atp < 0:
                                e_parts_needed_for_sellwish_p1[v] = 0
                            else:
                                e_parts_needed_for_sellwish_p1[v] = atp
                else:
                    p["sw_needed"] = sw_p1.quantity_1
        else:
            for p in product_1:
                p["sw_needed"] = 0

    p2 = Product2()

    # try to fix
    for p in product_2:
        for p_1 in product_1:
            if p["parts_number"] is p_1["parts_number"]:
                p["current_stock"] = (p["current_stock"] + p["orders_in_queue"] + p["orders_in_progress"]) - p_1["sw_needed"] - p_1["safety_stock"]


    sw_p2 = ProductionProgram.objects.get(product_id="P2")
    p2_ownparts = Ownparts.objects.get(item_name="P2")
    amount_to_produce_p2 = (sw_p2.quantity_1 + p2_ownparts.safety_stock) - p2_ownparts.quantity
    if amount_to_produce_p2 <= 0:
        e_parts_needed_for_sellwish_p2 = 0
    else:
        e_parts_needed_for_sellwish_p2 = p2.get_e_part_count(count=amount_to_produce_p2)
        if sw_p2.quantity_1 > 0:
            for p in product_2:
                if str(p["parts_number"]) != "P2":
                    p["sw_needed"] = e_parts_needed_for_sellwish_p2[p["parts_number"]]
                    if len(p2.get_e_part_count_for_e_part(p["parts_number"], count=1).keys()) > 0:
                        atp = (p["sw_needed"] + p["safety_stock"]) - (
                                p["current_stock"] + p["orders_in_queue"] + p["orders_in_progress"])
                        for v in p2.get_e_part_count_for_e_part(p["parts_number"], count=1).keys():
                            if atp < 0:
                                e_parts_needed_for_sellwish_p2[v] = 0
                            else:
                                e_parts_needed_for_sellwish_p2[v] = atp
                else:
                    p["sw_needed"] = sw_p2.quantity_1
        else:
            for p in product_2:
                p["sw_needed"] = 0

    p3 = Product3()

    # try to fix
    for p in product_3:
        for p_1 in product_1:
            if p["parts_number"] is p_1["parts_number"]:
                p["current_stock"] =(p["current_stock"] + p["orders_in_queue"] + p["orders_in_progress"]) - p_1["sw_needed"] - p_1["safety_stock"]
    for p in product_3:
        for p_2 in product_2:
            if p["parts_number"] is p_2["parts_number"]:
                p["current_stock"] = (p["current_stock"] + p["orders_in_queue"] + p["orders_in_progress"]) - p_2["sw_needed"] - p_2["safety_stock"]

    sw_p3 = ProductionProgram.objects.get(product_id="P3")
    p3_ownparts = Ownparts.objects.get(item_name="P3")
    amount_to_produce_p3 = (sw_p3.quantity_1 + p3_ownparts.safety_stock) - p3_ownparts.quantity
    if amount_to_produce_p3 <= 0:
        e_parts_needed_for_sellwish_p3 = 0
    else:
        e_parts_needed_for_sellwish_p3 = p3.get_e_part_count(count=amount_to_produce_p3)
        if sw_p3.quantity_1 > 0:
            for p in product_3:
                if str(p["parts_number"]) != "P3":
                    p["sw_needed"] = e_parts_needed_for_sellwish_p3[p["parts_number"]]
                    if len(p3.get_e_part_count_for_e_part(p["parts_number"], count=1).keys()) > 0:
                        atp = (p["sw_needed"] + p["safety_stock"]) - (
                                p["current_stock"] + p["orders_in_queue"] + p["orders_in_progress"])
                        for v in p3.get_e_part_count_for_e_part(p["parts_number"], count=1).keys():
                            if atp < 0:
                                e_parts_needed_for_sellwish_p3[v] = 0
                            else:
                                e_parts_needed_for_sellwish_p3[v] = atp
                else:
                    p["sw_needed"] = sw_p3.quantity_1
        else:
            for p in product_3:
                p["sw_needed"] = 0

    # calculate amount to produce
    for p in product_1:
        amount_to_produce = (p["sw_needed"] + p["safety_stock"]) - (
                p["current_stock"] + p["orders_in_queue"] + p["orders_in_progress"])
        if amount_to_produce < 0:
            amount_to_produce = 0
        p["amount_to_produce"] = amount_to_produce
        global AMOUNT_TO_PRODUCE
        AMOUNT_TO_PRODUCE.append((p['parts_number'], amount_to_produce))

    for p in product_2:
        amount_to_produce = (p["sw_needed"] + p["safety_stock"]) - (
                p["current_stock"] + p["orders_in_queue"] + p["orders_in_progress"])
        if amount_to_produce < 0:
            amount_to_produce = 0
        p["amount_to_produce"] = amount_to_produce
        AMOUNT_TO_PRODUCE.append((p['parts_number'], amount_to_produce))

    for p in product_3:
        amount_to_produce = (p["sw_needed"] + p["safety_stock"]) - (
                p["current_stock"] + p["orders_in_queue"] + p["orders_in_progress"])
        if amount_to_produce < 0:
            amount_to_produce = 0
        p["amount_to_produce"] = amount_to_produce
        AMOUNT_TO_PRODUCE.append((p['parts_number'], amount_to_produce))

    formset_ownparts = Formset_Ownparts()

    context = {
        'product_1': product_1,
        'product_2': product_2,
        'product_3': product_3,
        'formset_ownparts': formset_ownparts
    }
    return render(request, template, context)


def update_safety_stock(request):
    formset_ownparts = Formset_Ownparts(request.POST)
    if formset_ownparts.is_valid():
        ownparts = formset_ownparts.save(commit=False)
        for o in ownparts:
            o.save()
    return HttpResponseRedirect('/production-summary')
