from django.urls import path
from . import views
from ..sellwish_directsales import views as view_sell

urlpatterns = [
    path('purchase-orders', views.index, name='purchase_home'),
    path('add_to_db_function', views.add_to_db_function, name='add_to_db_function'),
    path('reset', views.reset, name='reset'),
    path('reset_application', view_sell.reset_application, name='reset_application')
]
