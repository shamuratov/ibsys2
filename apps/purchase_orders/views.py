from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import PurchaseOrders, get_columns_as_tuples_from_purchase_orders
from apps.production_orders.models import Buyparts
from django.forms import modelformset_factory, TextInput
from apps.sellwish_directsales.models import calculate_overall_consumption

Formset = modelformset_factory(
    PurchaseOrders,
    fields=('article', 'quantity', 'mode'),
    extra=0)

RESET_IS_ENABLED: bool = False


def index(request):
    """
    Purchase_Order-Objects from the DB will be rendered in HTML as a Form
    """
    template = 'purchase_orders/index.html'

    if request.method == 'POST':
        formset = Formset(request.POST)
        if formset.is_valid():
            formset.save()

    tmp_res = calculate_overall_consumption()  # dict with 29 elem: {1: [100, 100, 100, 100],...}

    buy_items = Buyparts.objects.all()
    production_data = []
    for b, prod in zip(buy_items, tmp_res.values()):
        # calculate total consumption sum needed over 4 periods
        elements_sum = (int(prod[0]) + int(prod[1]) + int(prod[2]) + int(prod[3]))
        elements_sum = 1 if elements_sum == 0 else elements_sum
        recommended_sum = elements_sum - b.quantity
        durability = round((b.quantity / (int(elements_sum) / 4)) - (b.delivery_time + b.delivery_deviation_time), 1)
        production_data.append({
            "1": prod[0],
            "2": prod[1],
            "3": prod[2],
            "4": prod[3],
            "durability": durability,
            "recommended_to_buy": recommended_sum
        })
        global RESET_IS_ENABLED
        if not RESET_IS_ENABLED:
            if recommended_sum < 0:
                continue
            if durability <= 0.1:
                PurchaseOrders.objects.filter(article=b.item_name).update(quantity=recommended_sum)
                PurchaseOrders.objects.filter(article=b.item_name).update(mode='Fast')
            else:
                PurchaseOrders.objects.filter(article=b.item_name).update(quantity=recommended_sum)

    formset = Formset()

    context = {
        'buy_items': buy_items,
        'formset': formset,
        'production_data': production_data
    }
    return render(request, template, context)


def add_to_db_function(request):
    # formset_sip = Formset_SellsInPeriod(request.POST or None)
    form_set = Formset(request.POST)
    # RESET_IS_ENABLED needed for UserInput, which would be overwritten by index()
    global RESET_IS_ENABLED
    RESET_IS_ENABLED = True
    if form_set.is_valid():
        formset = form_set.save(commit=False)
        for p in formset:
            p.save()
    return HttpResponseRedirect('/purchase-orders')


def reset(request):
    purchaseOrderObjects = list(PurchaseOrders.objects.all())
    global RESET_IS_ENABLED
    RESET_IS_ENABLED = True
    for p in purchaseOrderObjects:
        PurchaseOrders.objects.filter(article=p.article).update(quantity=0)
        PurchaseOrders.objects.filter(article=p.article).update(mode='Normal')
    return HttpResponseRedirect('/purchase-orders')
