from django.db import models
from enum import Enum


# Create your models here.
class ModeChoice(Enum):
    Fast = 4
    Normal = 5


class PurchaseOrders(models.Model):
    article = models.CharField(primary_key=True, max_length=4)
    quantity = models.PositiveIntegerField(default=0, null=True)
    mode = models.CharField(
        max_length=20,
        choices=[(tag.name, tag.value) for tag in ModeChoice],
        default=5,
        null=True
    )


def get_columns_as_tuples_from_purchase_orders() -> list:
    """
    returns contents of each row of PurchaseOrders table in form of tuple
    in this order: (article, quantity, modus)
    :return: f.e. [('21', '300', '5'),...]
    """
    result_list: list = []
    # assumption: each valid row has "quantity" AND "mode" filled
    # ~ SELECT * from PurchaseOrders where quantity IS NOT NULL AND mode IS NOT NULL
    filtered_results = PurchaseOrders.objects.exclude(quantity__isnull=True).exclude(mode__isnull=True)
    article_ids: list = [str(i[1:]) for i in filtered_results.values_list("article", flat=True)]
    quantity: list = [str(i) for i in filtered_results.values_list("quantity", flat=True)]
    mode: list = ["5" if i == "Normal" else "4" for i in filtered_results.values_list("mode", flat=True)]
    for ar, qu, mo in zip(article_ids, quantity, mode):
        if qu != "0":
            result_list.append((ar, qu, mo))
    return result_list
