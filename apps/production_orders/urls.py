from django.urls import path
from .import views
from ..sellwish_directsales import views as views_sell

urlpatterns = [
    path('production-orders', views.index, name='prod_home'),
    path('add_product_order', views.add_product_order, name='add_product_order'),
    path('update_product_order', views.update_product_order, name='update_product_order'),
    path('delete_product_order', views.delete_product_order, name='delete_product_order'),
    path('move_up', views.move_up, name='move_up'),
    path('move_down', views.move_down, name='move_down'),
    path('reset_application', views_sell.reset_application, name='reset_application'),
    path('split_product_order', views.split_product_order, name='split_product_order'),

]
