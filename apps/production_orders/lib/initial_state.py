# item_name: str, quantity: int, price:float, shipping_cost: int, discount_quantity: int
# delivery_time: float, delivery_deviation_time: float, {production_p1, production_p2, production_p3}: int
BUY_PARTS_DEFAULT_VALUES = [
    ("K21", 0, 0.0, 50, 300, 1.8, 0.4, 1, 0, 0),
    ("K22", 0, 0.0, 50, 300, 1.7, 0.4, 0, 1, 0),
    ("K23", 0, 0.0, 50, 300, 1.2, 0.2, 0, 0, 1),
    ("K24", 0, 0.0, 100, 6100, 3.2, 0.3, 7, 7, 7),
    ("K25", 0, 0.0, 50, 3600, 0.9, 0.2, 4, 4, 4),
    ("K27", 0, 0.0, 75, 1800, 0.9, 0.2, 2, 2, 2),
    ("K28", 0, 0.0, 50, 4500, 1.7, 0.4, 4, 5, 6),
    ("K32", 0, 0.0, 50, 2700, 2.1, 0.5, 3, 3, 3),
    ("K33", 0, 0.0, 75, 900, 1.9, 0.5, 0, 0, 2),
    ("K34", 0, 0.0, 50, 22000, 1.6, 0.3, 0, 0, 72),
    ("K35", 0, 0.0, 75, 3600, 2.2, 0.4, 4, 4, 4),
    ("K36", 0, 0.0, 100, 900, 1.2, 0.1, 1, 1, 1),
    ("K37", 0, 0.0, 50, 900, 1.5, 0.3, 1, 1, 1),
    ("K38", 0, 0.0, 50, 300, 1.7, 0.4, 1, 1, 1),
    ("K39", 0, 0.0, 75, 1800, 1.5, 0.3, 2, 2, 2),
    ("K40", 0, 0.0, 50, 900, 1.7, 0.2, 1, 1, 1),
    ("K41", 0, 0.0, 50, 900, 0.9, 0.2, 1, 1, 1),
    ("K42", 0, 0.0, 50, 1800, 1.2, 0.3, 2, 2, 2),
    ("K43", 0, 0.0, 75, 2700, 2.0, 0.5, 1, 1, 1),
    ("K44", 0, 0.0, 50, 900, 1.0, 0.2, 3, 3, 3),
    ("K45", 0, 0.0, 50, 900, 1.7, 0.3, 1, 1, 1),
    ("K46", 0, 0.0, 50, 900, 0.9, 0.3, 1, 1, 1),
    ("K47", 0, 0.0, 50, 900, 1.1, 0.1, 1, 1, 1),
    ("K48", 0, 0.0, 75, 1800, 1.0, 0.2, 2, 2, 2),
    ("K52", 0, 0.0, 50, 600, 1.6, 0.4, 2, 0, 0),
    ("K53", 0, 0.0, 50, 22000, 1.6, 0.2, 72, 0, 0),
    ("K57", 0, 0.0, 50, 600, 1.7, 0.3, 0, 2, 0),
    ("K58", 0, 0.0, 50, 22000, 1.6, 0.5, 0, 72, 0),
    ("K59", 0, 0.0, 50, 1800, 0.7, 0.2, 2, 2, 2)
]

# item_name: str, quantity: int, price: float, safety_stock: int
OWN_PARTS_DEFAULT_VALUES = [
    ("P3", 0, 0.0, 0),
    ("P2", 0, 0.0, 0),
    ("P1", 0, 0.0, 0),
    ("E9", 0, 0.0, 0),
    ("E8", 0, 0.0, 0),
    ("E7", 0, 0.0, 0),
    ("E6", 0, 0.0, 0),
    ("E56", 0, 0.0, 0),
    ("E55", 0, 0.0, 0),
    ("E54", 0, 0.0, 0),
    ("E51", 0, 0.0, 0),
    ("E50", 0, 0.0, 0),
    ("E5", 0, 0.0, 0),
    ("E49", 0, 0.0, 0),
    ("E4", 0, 0.0, 0),
    ("E31", 0, 0.0, 0),
    ("E30", 0, 0.0, 0),
    ("E29", 0, 0.0, 0),
    ("E26", 0, 0.0, 0),
    ("E20", 0, 0.0, 0),
    ("E19", 0, 0.0, 0),
    ("E18", 0, 0.0, 0),
    ("E17", 0, 0.0, 0),
    ("E16", 0, 0.0, 0),
    ("E15", 0, 0.0, 0),
    ("E14", 0, 0.0, 0),
    ("E13", 0, 0.0, 0),
    ("E12", 0, 0.0, 0),
    ("E11", 0, 0.0, 0),
    ("E10", 0, 0.0, 0),
]

# product_id: str, quantity: int, price: float, penalty: float
DIRECTSALES_DEFAULT_VALUES = [("P1", 0, 0, 0), ("P2", 0, 0, 0), ("P3", 0, 0, 0)]

# product_id: str, quantity_1: int, quantity_2: int, quantity_3: int, quantity_4: int
PRODUCTION_PROGRAM_DEFAULT_VALUES = [("P1", 0, 0, 0, 0), ("P2", 0, 0, 0, 0), ("P3", 0, 0, 0, 0)]

# product_id: str, quantity: int
SELL_WISH_DEFAULT_VALUES = [("P1", 0), ("P2", 0), ("P3", 0)]

# id: int, wp_id: int, shift: int, overtime: float, previous_overtime: float
# production_time_new: float, setup_time_new: float, total_time_needed: float
WORKING_PLACE_INITIAL_STATE = [
    (1, 1, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (2, 2, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (3, 3, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (4, 4, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (5, 5, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (6, 6, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (7, 7, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (8, 8, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (9, 9, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (10, 10, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (11, 11, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (12, 12, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (13, 13, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (14, 14, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
    (15, 15, 1, 0.0, 0.0, 0.0, 0.0, 0.0),
]

# article: str, quantity: int, mode: str
PURCHASE_ORDERS_INITIAL_STATE = [
    ("K21", 0, "Normal"),
    ("K22", 0, "Normal"),
    ("K23", 0, "Normal"),
    ("K24", 0, "Normal"),
    ("K25", 0, "Normal"),
    ("K27", 0, "Normal"),
    ("K28", 0, "Normal"),
    ("K32", 0, "Normal"),
    ("K33", 0, "Normal"),
    ("K34", 0, "Normal"),
    ("K35", 0, "Normal"),
    ("K36", 0, "Normal"),
    ("K37", 0, "Normal"),
    ("K38", 0, "Normal"),
    ("K39", 0, "Normal"),
    ("K40", 0, "Normal"),
    ("K41", 0, "Normal"),
    ("K42", 0, "Normal"),
    ("K43", 0, "Normal"),
    ("K44", 0, "Normal"),
    ("K45", 0, "Normal"),
    ("K46", 0, "Normal"),
    ("K47", 0, "Normal"),
    ("K48", 0, "Normal"),
    ("K52", 0, "Normal"),
    ("K53", 0, "Normal"),
    ("K57", 0, "Normal"),
    ("K58", 0, "Normal"),
    ("K59", 0, "Normal"),
]


def reset_lager_to_initial_state():
    from apps.production_orders.models import Ownparts, Buyparts
    for row in BUY_PARTS_DEFAULT_VALUES:
        Buyparts.objects.filter(item_name=row[0]).update(
            quantity=row[1],
            price=row[2],
            shipping_cost=row[3],
            discount_quantity=row[4],
            delivery_time=row[5],
            delivery_deviation_time=row[6],
            production_p1=row[7],
            production_p2=row[8],
            production_p3=row[9],
        )

    for row in OWN_PARTS_DEFAULT_VALUES:
        Ownparts.objects.filter(item_name=row[0]).update(
            quantity=row[1],
            price=row[2],
            safety_stock=row[3]
        )


def reset_sellwish_directsales_to_initial_state():
    from apps.sellwish_directsales.models import Sellwish, Directsales, ProductionProgram
    for row in DIRECTSALES_DEFAULT_VALUES:
        Directsales.objects.filter(product_id=row[0]).update(
            quantity=row[1],
            price=row[2],
            penalty=row[3]
        )
    for row in PRODUCTION_PROGRAM_DEFAULT_VALUES:
        ProductionProgram.objects.filter(product_id=row[0]).update(
            quantity_1=row[1],
            quantity_2=row[2],
            quantity_3=row[3],
            quantity_4=row[4]
        )
    for row in SELL_WISH_DEFAULT_VALUES:
        Sellwish.objects.filter(product_id=row[0]).update(
            quantity=row[1]
        )


def reset_working_place_to_initial_state():
    from apps.capacity_requirements.models import Workingplace
    for row in WORKING_PLACE_INITIAL_STATE:
        Workingplace.objects.filter(id=row[0]).update(
            wp_id=row[1],
            shift=row[2],
            overtime=row[3],
            previous_overtime=row[4],
            production_time_new=row[5],
            setup_time_new=row[6],
            total_time_needed=row[7]
        )


def reset_production_orders_to_initial_state():
    from apps.production_orders.models import ProductOrder
    _ids = list(ProductOrder.objects.values_list("id", flat=True))
    _list_of_zeros = [0] * len(_ids)
    for x, y in zip(_ids, _list_of_zeros):
        ProductOrder.objects.filter(id=x).update(quantity=y)
    from apps.production_orders.views import _TEMP_PRODUCTION_ORDERS_STATE
    _TEMP_PRODUCTION_ORDERS_STATE.clear()


def reset_purchase_orders_to_initial_state():
    from apps.purchase_orders.models import PurchaseOrders
    for row in PURCHASE_ORDERS_INITIAL_STATE:
        PurchaseOrders.objects.filter(article=row[0]).update(quantity=row[1], mode=row[2])


def reset_all():
    reset_sellwish_directsales_to_initial_state()
    reset_production_orders_to_initial_state()
    reset_purchase_orders_to_initial_state()
    reset_lager_to_initial_state()
    reset_working_place_to_initial_state()

    from shutil import copyfile
    copyfile("db.sqlite3_original", "db.sqlite3")

