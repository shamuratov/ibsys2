from django.apps import AppConfig


class ProductionOrdersConfig(AppConfig):
    name = 'production_orders'
