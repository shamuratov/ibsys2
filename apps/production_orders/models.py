from django.db import models
from django.db.models import F
from enum import Enum


class OwnPartsEnum(Enum):
    P3 = "P3"
    P2 = "P2"
    P1 = "P1"
    E9 = "E9"
    E8 = "E8"
    E7 = "E7"
    E6 = "E6"
    E5 = "E5"
    E4 = "E4"
    E56 = "E56"
    E55 = "E55"
    E54 = "E54"
    E51 = "E51"
    E50 = "E50"
    E49 = "E49"
    E31 = "E31"
    E30 = "E30"
    E29 = "E29"
    E26 = "E26"
    E20 = "E20"
    E19 = "E19"
    E18 = "E18"
    E17 = "E17"
    E16 = "E16"
    E15 = "E15"
    E14 = "E14"
    E13 = "E13"
    E12 = "E12"
    E11 = "E11"
    E10 = "E10"


class Buyparts(models.Model):
    item_name = models.CharField(primary_key=True, max_length=4)  # K1 .. K2
    quantity = models.PositiveIntegerField(default=0)
    price = models.FloatField(default=0)
    shipping_cost = models.PositiveIntegerField(default=0)
    discount_quantity = models.PositiveIntegerField(default=0)
    delivery_time = models.FloatField(default=0)
    delivery_deviation_time = models.FloatField(default=0)
    production_p1 = models.PositiveIntegerField(default=0)
    production_p2 = models.PositiveIntegerField(default=0)
    production_p3 = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "%s %s %s %s %s %s %s %s %s %s" % (
            self.item_name,
            self.quantity,
            self.price,
            self.shipping_cost,
            self.discount_quantity,
            self.delivery_time,
            self.delivery_deviation_time,
            self.production_p1,
            self.production_p2,
            self.production_p3)


class Ownparts(models.Model):
    item_name = models.CharField(primary_key=True, max_length=4)  # E3 .. E3
    quantity = models.PositiveIntegerField(default=0)
    price = models.FloatField(default=0)
    safety_stock = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "%s %s %s" % (self.item_name, self.quantity, self.price)


class ProductOrder(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    item_name = models.CharField(max_length=4)
    quantity = models.PositiveIntegerField(default=0)


def get_df_from_buyparts_production_data():
    """
    this function is needed for matrix multiplication on purchase page
    :return: pandas dataframe for further calculation
    """
    import pandas as pd
    data_dict = dict()
    data_dict[0] = list(Buyparts.objects.values_list('production_p1', flat=True))
    data_dict[1] = list(Buyparts.objects.values_list('production_p2', flat=True))
    data_dict[2] = list(Buyparts.objects.values_list('production_p3', flat=True))
    return pd.DataFrame.from_dict(data=data_dict)


def get_dict_item_quantity_from_ownparts() -> dict:
    """
    return from db dict in form {"item_name": quantity}
    :return: f.e {'E10': 0, 'E11': 0,...}
    """
    return dict(Ownparts.objects.values_list("item_name", "quantity"))


def get_dict_item_quantity_from_productorders() -> dict:
    """
    return from db dict of NOT empty ProductOrders in form {"item_name": quantity}
    :return: f.e {'E10': 10, 'E11': 20,...}
    """
    return dict(ProductOrder.objects.exclude(quantity__exact=0).values_list("item_name", "quantity"))


def get_item_amount_tuples_list_for_ownparts(items_list: list) -> list:
    """
    for a given list of article_ids returns tuples list with (item_name, quantity)
    :param items_list:
    :return:
    """
    return list(Ownparts.objects.filter(item_name__in=items_list).values_list("item_name", "quantity"))


def get_item_amount_tuples_list_for_buyparts(items_list: list) -> list:
    """
    for a given list of article_ids returns tuples list with (item_name, quantity)
    :param items_list:
    :return:
    """
    return list(Buyparts.objects.filter(item_name__in=items_list).values_list("item_name", "quantity"))


def convert_int_item_to_string(item_name: int) -> str:
    """
    takes for example 1, returns "P1", 4 -> "E4", 21 -> "K21"
    :param item_name: item_name
    :return: string version of item_name compatible with databases values
    """
    available_e_p_parts = list(Ownparts.objects.values_list("item_name", flat=True))
    available_k_parts = list(Buyparts.objects.values_list("item_name", flat=True))
    if type(item_name) == str:
        item_name = int(item_name[1:])

    for e_p in available_e_p_parts:
        if int(e_p[1:]) == item_name:
            return e_p
    for k in available_k_parts:
        if int(k[1:]) == item_name:
            return k


def substract_direct_sales_from_lager():
    """
    find and substract values in direct sales from ownparts
    :return:
    """
    from apps.sellwish_directsales.models import Directsales
    possible_amount = []
    product_ids = ["P1", "P2", "P3"]
    for pid in product_ids:
        needed_amount = Directsales.objects.filter(product_id=pid).values_list("quantity", flat=True).get()
        available_amount = Ownparts.objects.filter(item_name=pid).values_list("quantity", flat=True).get()
        if needed_amount > available_amount:
            possible_amount.append(available_amount)
        else:
            possible_amount.append(needed_amount)
    for x, y in zip(product_ids, possible_amount):
        (Ownparts.objects.filter(item_name=x).update(quantity=F('quantity') - y))


def create_matrix_for_production_calculation():
    """
    this matrix is needed for production_order matrix multiplication
    returns dictionary with keys: Models 'item_names', and list of
    multiplication coefficients as values
    F.e. {'K21': [1, 0, 0],...}
    """
    from _collections import OrderedDict
    from .models import Buyparts
    _item_name = list()
    _production_p1 = list()
    _production_p2 = list()
    _production_p3 = list()
    corresponding_matrix = OrderedDict()

    for pair_0 in Buyparts.objects.values('item_name'):
        for v in pair_0.values():
            _item_name.append(v)

    for pair_1 in Buyparts.objects.values('production_p1'):
        for v in pair_1.values():
            _production_p1.append(v)

    for pair_2 in Buyparts.objects.values('production_p2'):
        for v in pair_2.values():
            _production_p2.append(v)

    for pair_3 in Buyparts.objects.values('production_p3'):
        for v in pair_3.values():
            _production_p3.append(v)

    for item, it_1, it_2, it_3 in zip(
            _item_name, _production_p1, _production_p2, _production_p3):
        corresponding_matrix[item] = [it_1, it_2, it_3]
    return corresponding_matrix


def get_single_value_for_buyparts_column(column_name: str, item_name: str):
    """
    :param column_name: legit are all columns available in db table Buyparts, f.e. "quantity"
    :param item_name: legit are all values in Buyparts.item_name, f.e. 21 or "K21"
    :return: single value for any column in Buypart
    """
    item_name = convert_int_item_to_string(item_name) if type(item_name) == int else item_name
    available_k_parts = Buyparts.objects.values_list("item_name", flat=True)
    if item_name in available_k_parts:
        value = Buyparts.objects.values_list(column_name, flat=True).get(item_name=item_name)
    return value


def get_single_value_for_ownparts_column(column_name: str, item: str):
    """
    :param column_name: legit are all columns available in db table Ownparts, f.e. "quantity"
    :param item: legit are all values in Ownparts.item_name, f.e. 5 or "E5"
    :return: single value for any column in Ownparts
    """
    item_name = convert_int_item_to_string(item) if type(item) == int else item
    available_e_p_parts = Ownparts.objects.values_list("item_name", flat=True)
    if item_name in available_e_p_parts:
        value = Ownparts.objects.values_list(column_name, flat=True).get(item_name=item)
    return value