from django.contrib import admin

from .models import Buyparts, Ownparts


class BuypartsInline(admin.TabularInline):
    model = Buyparts
    extra = 2


class BuypartsAdmin(admin.ModelAdmin):
    fields = [
        "item_name", "quantity", "price", "shipping_cost", "discount_quantity",
        "delivery_time", "delivery_deviation_time", "production_p1",
        "production_p2", "production_p3"
    ]
    list_display = (
        "item_name", "quantity", "price", "shipping_cost",
        "discount_quantity", "delivery_time", "delivery_deviation_time",
        "production_p1", "production_p2", "production_p3")


admin.site.register(Buyparts, BuypartsAdmin)
admin.site.register(Ownparts)
