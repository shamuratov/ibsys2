from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import ProductOrder
from django.forms import modelformset_factory, TextInput
from apps.purchase_summary.views import AMOUNT_TO_PRODUCE

Formset = modelformset_factory(
    ProductOrder, fields=('id', 'item_name', 'quantity'),

    widgets={"id": TextInput(attrs={'readonly': True, 'style': 'width: 30px'})}, extra=0)

_TEMP_PRODUCTION_ORDERS_STATE = []


def index(request):
    """
    Product_Order-Objects from the DB will be rendered in HTML as a Form
    """
    if not request.GET.get('with_split'):
        if not _TEMP_PRODUCTION_ORDERS_STATE:
            for pairs in AMOUNT_TO_PRODUCE:
                ProductOrder.objects.filter(item_name=pairs[0]).update(quantity=pairs[1])

    if request.GET.get('from_summary'):
        # work here with unperformant to not loose order
        summary_order_list = []
        summary_data_dict = {}
        for _o in ProductOrder.objects.all():
            if _o.item_name in summary_data_dict.keys():
                summary_data_dict[_o.item_name].quantity = _o.quantity
            else:
                summary_order_list.append(_o.item_name)
                summary_data_dict[_o.item_name] = _o
        ProductOrder.objects.all().delete()
        for idx, _e in enumerate(summary_order_list):
            _o = summary_data_dict.get(_e)
            _o.id = idx
            _o.save()

    template = 'production_orders/index.html'

    if request.method == 'POST':
        formset = Formset(request.POST)
        if formset.is_valid():
            formset.save()

    formset = Formset()
    context = {
        'formset': formset
    }
    return render(request, template, context)


def move_up(request):
    _id = int(request.GET.get('up_id'))
    _id_needed = _id - 1

    if _id_needed >= 0:
        pre_item = list(ProductOrder.objects.filter(id=_id).values("item_name", "quantity"))
        post_item = list(ProductOrder.objects.filter(id=_id_needed).values("item_name", "quantity"))

        # takes dictionary out of list
        product_order_clicked = {k: v for d in pre_item for k, v in d.items()}
        product_order_swap = {k: v for d in post_item for k, v in d.items()}
        ProductOrder.objects.filter(id=_id).update(item_name=product_order_swap.get("item_name"),
                                                   quantity=product_order_swap.get("quantity"))
        ProductOrder.objects.filter(id=_id_needed).update(item_name=product_order_clicked.get("item_name"),
                                                          quantity=product_order_clicked.get("quantity"))
    return HttpResponseRedirect('production-orders')


def move_down(request):
    _id = int(request.GET.get('down_id'))
    _id_needed = _id + 1
    max_length = len(list(ProductOrder.objects.all()))

    if _id_needed <= max_length:
        product_order_clicked = ProductOrder.objects.get(id=_id)
        product_order_swap = ProductOrder.objects.get(id=_id_needed)
        ProductOrder.objects.filter(id=_id).update(item_name=getattr(product_order_swap, 'item_name'),
                                                   quantity=getattr(product_order_swap, 'quantity'))
        ProductOrder.objects.filter(id=_id_needed).update(item_name=getattr(product_order_clicked, 'item_name'),
                                                          quantity=getattr(product_order_clicked, 'quantity'))
    return HttpResponseRedirect('production-orders')


def add_product_order():
    last_id = 0
    for po in ProductOrder.objects.all():
        if po.id >= last_id:
            last_id = po.id
    new_id = last_id + 1
    new_product_order = ProductOrder(new_id, None, '0')  # replace zero in the middle through the new valid item_name
    new_product_order.save()
    return HttpResponseRedirect('production-orders')


def update_product_order(request):
    """
    to the given id writes appropriate item_name, quantity
    :param request:
    :return:
    """
    formset_product_order = Formset(request.POST)
    global _TEMP_PRODUCTION_ORDERS_STATE
    _TEMP_PRODUCTION_ORDERS_STATE.clear()
    if formset_product_order.is_valid():
        production = formset_product_order.save(commit=False)
        for prod in production:
            prod.save(force_update=True)
        _TEMP_PRODUCTION_ORDERS_STATE = list(ProductOrder.objects.values("id", "item_name", "quantity"))
    return HttpResponseRedirect('production-orders')


def delete_product_order(request):
    """TODO: rewrite logic
    GOAL: to have consequent index queue
    store all ids above and below _id
    rewrite data in db above and below
    """
    global _TEMP_PRODUCTION_ORDERS_STATE
    _TEMP_PRODUCTION_ORDERS_STATE.clear()

    _id = request.GET.get('delete_id')
    ProductOrder.objects.filter(id=_id).delete()

    _TEMP_PRODUCTION_ORDERS_STATE = list(ProductOrder.objects.values("id", "item_name", "quantity"))
    return HttpResponseRedirect('production-orders')


def split_product_order_put_in_end():
    """
    takes data received from pop-up window and insert in the bottom of the table
    :param request:
    :return:
    """
    new_id = len(list(ProductOrder.objects.all())) + 1
    _article_id = "P1"
    _quantity = 1
    ProductOrder(new_id, _article_id, _quantity).save()
    # return HttpResponseRedirect('production-orders')


def split_product_order(request):
    _given_id = int(request.GET.get('id'))
    _old_quantity = int(request.GET.get('quantity'))
    _given_quantity = int(request.GET.get('new_quantity'))
    _given_item_name = request.GET.get('item_name')
    _old_state_po = list(ProductOrder.objects.all().values())
    _values_under_split = _old_state_po[_given_id + 1:].copy()

    if _old_quantity - _given_quantity < 0:
        return HttpResponseRedirect('production-orders')

    # update the existing row
    ProductOrder.objects.filter(id=_given_id).update(quantity=int(_old_quantity - _given_quantity))

    # save the new row
    _o = ProductOrder()
    _o.id = _given_id + 1
    _o.item_name = _given_item_name
    _o.quantity = _given_quantity
    _o.save()

    # shift all rows below split down, override the existing values
    if _values_under_split:
        for idx, row in enumerate(_values_under_split):
            _obj = ProductOrder()
            _obj.id = _o.id + (idx + 1)
            _obj.item_name = row["item_name"]
            _obj.quantity = row["quantity"]
            _obj.save()

    return HttpResponseRedirect('production-orders?with_split=true')
