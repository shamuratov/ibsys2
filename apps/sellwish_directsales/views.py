from apps.production_orders.lib.initial_state import reset_all
from apps.sellwish_directsales.core_logic.logic import is_sufficient_lager, calculate_minimum_possible_amount_to_produce
from django.forms import modelformset_factory, TextInput
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db.models import F
from .models import ProductionProgram, Directsales, Sellwish
from .forms import UploadFileForm
from apps.production_orders.models import (Buyparts, Ownparts, convert_int_item_to_string,
                                           get_item_amount_tuples_list_for_ownparts,
                                           get_item_amount_tuples_list_for_buyparts)
from apps.capacity_requirements.models import Workingplace
from utils.products.Product import Product
from utils.products.Product1 import Product1
from utils.products.Product2 import Product2
from utils.products.Product3 import Product3
from utils.parse_input_xml import readinput
from apps.production_orders.lib.production_cycle import PRODUCTION_CYCLE
import xml

LIST_OF_PRODUCTS = [1, 2, 3]
LIST_OF_OWN_PARTS = [
    4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 26, 29, 30, 31, 49, 50, 51, 54, 55, 56
]
file_not_uploaded = True

Formset_ProductionProgram = modelformset_factory(
    ProductionProgram,
    fields=('product_id', 'quantity_1', 'quantity_2', 'quantity_3', 'quantity_4'),
    widgets={"product_id": TextInput(attrs={'readonly': True, })},
    extra=0)

Formset_Directsales = modelformset_factory(
    Directsales,
    fields=('product_id', 'quantity', 'price', 'penalty'),
    widgets={"product_id": TextInput(attrs={'readonly': True, })},
    extra=0)

Formset_SellsInPeriod = modelformset_factory(
    Sellwish,
    fields=('product_id', 'quantity'), widgets={"product_id": TextInput(attrs={'readonly': True, })},
    extra=0)


def write_warehouse_stock_to_db(revert=False):
    """
    reads data from given dictionary,
    iterate paralelly over values of this dictionary
    and store price and quantity over the value in db
    tables: Ownparts and Buyparts
    """
    if revert:
        pass
    else:
        result = zip(WAREHOUSE_STOCK["parts_ids"],
                     WAREHOUSE_STOCK["parts_amount"],
                     WAREHOUSE_STOCK["parts_prices"])

        for iid, new_quantity, new_price in list(result):
            if iid in LIST_OF_PRODUCTS:
                (Ownparts.objects
                 .filter(item_name="P" + str(iid))
                 .update(quantity=new_quantity, price=new_price))
            elif iid in LIST_OF_OWN_PARTS:
                (Ownparts.objects
                 .filter(item_name="E" + str(iid))
                 .update(quantity=new_quantity, price=new_price))
            else:
                (Buyparts.objects
                 .filter(item_name="K" + str(iid))
                 .update(quantity=new_quantity, price=new_price))


def write_waiting_list_workstations_to_db(revert: bool = False):
    """
    reads data from given dictionary, iterate simultaneously over values of this dictionary
    and stores overtime from the previous period to
    the table Workingplace[previous_overtime]
    :param revert: if true - sets back to previous value
    :return:
    """
    if revert:
        pass
    else:
        needed_e_parts = {}
        needed_k_parts = {}
        # core computation logic starts here
        for workplace_id, prev_overtime, own_item, amnt in zip(
                WAITING_LIST_WORKSTATIONS["workplace_id"], WAITING_LIST_WORKSTATIONS["timeneed"],
                WAITING_LIST_WORKSTATIONS["item"], WAITING_LIST_WORKSTATIONS["amount"]):
            valid_own_item = convert_int_item_to_string(own_item)

            # defines full production chain for a given Ownpart item
            wp_prodtime_setup_tupleslist: list = PRODUCTION_CYCLE.get(valid_own_item)

            # prod_coefficients dictionary defines how many Ownparts and Buyparts are needed to produce one Ownpart
            prod_coefficients: dict = {}
            for tup in wp_prodtime_setup_tupleslist:
                if tup[0] == workplace_id and len(tup) == 4:
                    prod_coefficients = tup[3]
            available_k_amnt = dict(get_item_amount_tuples_list_for_buyparts(list(prod_coefficients.keys())))
            available_e_amnt = dict(get_item_amount_tuples_list_for_ownparts(list(prod_coefficients.keys())))
            for prod_k, prod_v in prod_coefficients.items():
                if prod_k in available_e_amnt.keys():
                    needed_e_parts[prod_k] = prod_coefficients.get(prod_k) * amnt
                else:
                    needed_k_parts[prod_k] = prod_coefficients.get(prod_k) * amnt

            """
            is_enough: bool = is_sufficient_lager(available_k_amnt, needed_k_parts, available_e_amnt, needed_e_parts)
            min_possible: int = calculate_minimum_possible_amount_to_produce(available_k_amnt, available_e_amnt,
                                                                             prod_coefficients)
            # if yes: substract subcomponents from Buyparts and Ownparts and then add this amount to Ownparts table
            if is_enough:
                for nek, nev in needed_e_parts.items():
                    Ownparts.objects.filter(item_name=nek).update(quantity=F('quantity') - nev)
                for nkk, nkv in needed_k_parts.items():
                    Buyparts.objects.filter(item_name=nkk).update(quantity=F('quantity') - nkv)
                Ownparts.objects.filter(item_name=valid_own_item).update(quantity=F('quantity') + amnt)
                # for a specific workplace adds previous_overtime
                (Workingplace.objects.filter(wp_id=workplace_id)
                 .update(previous_overtime=F('previous_overtime') + prev_overtime))
            # if no: substract components only for available amount
            else:
                possible_e_parts, possible_k_parts = possible_to_produce(min_possible, prod_coefficients)
                for nek, nev in possible_e_parts.items():
                    Ownparts.objects.filter(item_name=nek).update(quantity=F('quantity') - nev)
                for nkk, nkv in possible_k_parts.items():
                    Buyparts.objects.filter(item_name=nkk).update(quantity=F('quantity') - nkv)
                Ownparts.objects.filter(item_name=valid_own_item).update(quantity=F('quantity') + min_possible)
                for x in wp_prodtime_setup_tupleslist:
                    if x[0] == workplace_id:
                        (Workingplace.objects.filter(wp_id=workplace_id)
                         .update(previous_overtime=F('previous_overtime') + x[1] * min_possible))
                # calculate additional production time on other workingplaces from PRODUCTION_CYCLE
                # for given item:
                #   take production_time multiply with amount and add to according workingplace["previous_overtime"]
            """

            wp_queue = [i[0] for i in wp_prodtime_setup_tupleslist]
            if wp_queue[-1] != workplace_id:
                for x in range(wp_queue.index(workplace_id), len(wp_prodtime_setup_tupleslist)):
                    (Workingplace.objects
                     .filter(wp_id=wp_prodtime_setup_tupleslist[x][0])
                     .update(previous_overtime=F("previous_overtime") + wp_prodtime_setup_tupleslist[x][1] * amnt))
            needed_k_parts.clear()
            needed_e_parts.clear()


def write_orders_in_work_to_workingplace_db():
    """
    reads data from given dictionary,
    iterate paralelly over values of this dictionary and stores overtime
    from the previous period to the table Workingplace[previous_overtime]
    """
    result = zip(ORDERS_IN_WORK["workplace_id"], ORDERS_IN_WORK["timeneed"])
    for workplace_id, prev_overtime in list(result):
        (Workingplace.objects
         .filter(wp_id=workplace_id)
         .update(previous_overtime=F('previous_overtime') + prev_overtime))


def get_purchase_summary_data(product=1):
    """
    part_number part_description safety_stock current_stock orders_in_queue orders_in_progress
    select correct Product
    :param product:
    :return:
    """

    p = Product()
    if product is 3:
        p = Product3()
    elif product is 2:
        p = Product2()
    else:
        p = Product1()

    parts_number = list(p.get_e_part_count(count=1).keys())
    parts_number.append("P" + str(p.id))

    summary_list = []
    for current_e_part in parts_number:
        # set defaults to show no None's in view
        # safety_stock should be always 0 here and parts_description is always generated the same way
        parts_description = current_e_part + " ownpart"
        orders_in_queue = 0
        orders_in_progress = 0

        # extract stock at the end of the previous period
        o = Ownparts.objects.get(item_name=current_e_part)
        current_stock: int = o.quantity
        safety_stock: int = o.safety_stock

        # extract orders in queue
        result = zip(WAITING_LIST_WORKSTATIONS["item"],
                     WAITING_LIST_WORKSTATIONS["amount"])
        for item, amount in result:
            if item > 3:
                if str("E" + str(item)) == str(current_e_part):
                    orders_in_queue += amount
            else:
                if str("P" + str(item)) == str(current_e_part):
                    orders_in_queue += amount

        # extract orders in work
        result = zip(ORDERS_IN_WORK["item"],
                     ORDERS_IN_WORK["amount"])
        for item, amount in result:
            if item > 3:
                if str("E" + str(item)) == str(current_e_part):
                    orders_in_progress += amount
            else:
                if str("P" + str(item)) == str(current_e_part):
                    orders_in_progress += amount

        summary_list.append({
            "parts_number": current_e_part,
            "parts_description": parts_description,
            "safety_stock": safety_stock,
            "current_stock": current_stock,
            "orders_in_queue": orders_in_queue,
            "orders_in_progress": orders_in_progress,
            "sw_needed": 0,
        })

    return summary_list


def write_sellwish_to_prod_program():
    """
    automatically saves amount given to Sellwish to Production Program amount
    :return: None
    """
    src = list(Sellwish.objects.values_list('product_id', 'quantity'))
    for _src in src:
        ProductionProgram.objects.filter(product_id=_src[0]).update(quantity_1=_src[1])


def write_the_rest_to_safety_stock():
    """
    calculate the difference between Production Program and Sellwish. If it is >0 write it to safety_stock
    todo: if Production Program < Sellwish then, check if it subtract diff from Ownparts quantity
    :return: None
    """

    sellwishes = list(Sellwish.objects.values_list('product_id', 'quantity'))
    prod_prog = list(ProductionProgram.objects.values_list('product_id', 'quantity_1'))
    diff = []
    for _s, _pp in zip(sellwishes, prod_prog):
        if _s[0] == _pp[0] and (_pp[1] - _s[1]) > 0:
            diff.append((_s[0], _pp[1] - _s[1]))
    for i_pair in diff:
        Ownparts.objects.filter(item_name=i_pair[0]).update(safety_stock=i_pair[1])


def handle_uploaded_file(f):
    """
    Handle db save of sellwish and direct orders here and refresh page
    :param f:
    :return:
    """
    tree = xml.etree.ElementTree.parse(f)

    _WAREHOUSE_STOCK, _WAITING_LIST_STOCK, _WAITING_LIST_WORKSTATIONS, _ORDERS_IN_WORK = readinput(tree)

    global WAREHOUSE_STOCK
    WAREHOUSE_STOCK = _WAREHOUSE_STOCK

    global WAITING_LIST_STOCK
    WAITING_LIST_STOCK = _WAITING_LIST_STOCK

    global WAITING_LIST_WORKSTATIONS
    WAITING_LIST_WORKSTATIONS = _WAITING_LIST_WORKSTATIONS

    global ORDERS_IN_WORK
    ORDERS_IN_WORK = _ORDERS_IN_WORK

    write_warehouse_stock_to_db()
    write_orders_in_work_to_workingplace_db()
    write_waiting_list_workstations_to_db()

    global file_not_uploaded
    file_not_uploaded = False


def upload_file(request):
    template = 'sellwish_directsales/index.html'
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['input_xml'])
            return HttpResponseRedirect('/')
    else:
        form = UploadFileForm()
    return render(request, template, {'form': form})


def reset_application(request):
    """
    resets the db tables to default state
    :param request:
    :return:
    """
    try:
        reset_all()
    except LookupError:
        return HttpResponseRedirect('/')

    global file_not_uploaded
    file_not_uploaded = True
    return HttpResponseRedirect('/')


def index(request):
    """
    Sellwish/Direct_sales-Objects from the db will be rendered in HTML as a Form
    :param request:
    :return:
    """
    template = 'sellwish_directsales/index.html'
    formset_prodprogramm = Formset_ProductionProgram()
    formset_directsales = Formset_Directsales()
    formset_sellwish = Formset_SellsInPeriod()
    context = {
        'formset_sellwish': formset_sellwish,
        'formset_prodprogramm': formset_prodprogramm,
        'formset_directsales': formset_directsales,
        'file_not_uploaded': file_not_uploaded,
    }
    return render(request, template, context)


def update_production_program_form(request):
    formset_sellwish = Formset_ProductionProgram(request.POST)
    if formset_sellwish.is_valid():
        sell_wishes = formset_sellwish.save(commit=False)
        for sell in sell_wishes:
            sell.save()
        write_the_rest_to_safety_stock()
    return HttpResponseRedirect('/')


def update_directsales_form(request):
    formset_directsales = Formset_Directsales(request.POST or None)
    if formset_directsales.is_valid():
        direct_sales = formset_directsales.save(commit=False)
        for dsell in direct_sales:
            dsell.save()
    return HttpResponseRedirect('/')


def update_sellsinperiod_form(request):
    formset_sells_in_period = Formset_SellsInPeriod(request.POST or None)
    if formset_sells_in_period.is_valid():
        sells_in_period = formset_sells_in_period.save(commit=False)
        for sip in sells_in_period:
            sip.save()
        write_sellwish_to_prod_program()
    return HttpResponseRedirect('/')
