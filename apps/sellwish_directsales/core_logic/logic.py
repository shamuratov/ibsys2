def is_sufficient_lager(available_k_amount: dict, needed_k_amount: dict,
                        available_e_amount: dict, needed_e_amount: dict) -> bool:
    """
    PREREQUISITES: Dictionaries for K-Parts have equal size and keys.
    Dictionaries for E-Parts also have the equal size and keys
    :param available_k_amount:
    :param needed_k_amount:
    :param available_e_amount:
    :param needed_e_amount:
    :return: boolean that says if we have enough articles to produce a part
    """
    if available_e_amount.keys() == needed_e_amount.keys() and len(available_e_amount) == len(needed_e_amount):
        for need_ek, need_ev in needed_e_amount.items():
            if need_ev > available_e_amount.get(need_ek):
                return False
    if available_k_amount.keys() == needed_k_amount.keys() and len(available_k_amount) == len(needed_k_amount):
        for need_kk, need_kv in needed_k_amount.items():
            if need_kv > available_k_amount.get(need_kk):
                return False
    return True


def calculate_minimum_possible_amount_to_produce(available_k_amount: dict,
                                                 available_e_amount: dict,
                                                 prod_coefficients: dict) -> int:
    """
    for each production coefficient pair checks how many items can be produced
    and store result of their amount division in a list. After that
    returns minimum of this list
    :param available_k_amount: {article: amount} key-value pair
    :param available_e_amount: {article: amount} key-value pair
    :param prod_coefficients: {article: amount} key-value pair with ratio
    :return: int - minimum possible amount that can be produced
    """
    output_list = []
    for prod_k, prod_v in prod_coefficients.items():
        if prod_k in available_e_amount.keys():
            output_list.append(available_e_amount.get(prod_k) / prod_v)
        else:
            output_list.append(available_k_amount.get(prod_k) / prod_v)
    if len(output_list) == 0:
        return 0
    return int(min(output_list)) or 0


def possible_to_produce(amount: int, prod_coefficients: dict):
    """
    calculates how many is possible to produce with various coefficients and amount
    :param amount:
    :param prod_coefficients:
    :return:
    """
    possible_k_output = {}
    possible_e_output = {}
    for prod_k, prod_v in prod_coefficients.items():
        if prod_k.startswith("K"):
            possible_k_output[prod_k] = amount * prod_v
        else:
            possible_e_output[prod_k] = amount * prod_v
    return possible_e_output, possible_k_output
