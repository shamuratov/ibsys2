from django.contrib import admin

from .models import ProductionProgram, Directsales, Sellwish

# Register your models here.
admin.site.register(ProductionProgram)
admin.site.register(Directsales)
admin.site.register(Sellwish)
