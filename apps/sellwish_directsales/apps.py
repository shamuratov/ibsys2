from django.apps import AppConfig


class SellwishDirectsalesConfig(AppConfig):
    name = 'sellwish_directsales'
