from django import forms


class UploadFileForm(forms.Form):
    input_xml = forms.FileField()
