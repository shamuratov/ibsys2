from django.db import models


class Sellwish(models.Model):
    product_id = models.CharField(primary_key=True, max_length=4)
    quantity = models.PositiveIntegerField()


class ProductionProgram(models.Model):
    product_id = models.CharField(primary_key=True, max_length=4)
    quantity_1 = models.PositiveIntegerField()
    quantity_2 = models.PositiveIntegerField()
    quantity_3 = models.PositiveIntegerField()
    quantity_4 = models.PositiveIntegerField()


class Directsales(models.Model):
    product_id = models.CharField(primary_key=True, max_length=4)
    quantity = models.PositiveIntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    penalty = models.DecimalField(max_digits=10, decimal_places=2)


def get_df_from_sellwish_data():
    """
    this function is needed for matrix multiplication on purchase page
    :return: pandas dataframe for further calculation
    """
    import pandas as pd
    data_dict = dict()
    data_dict[0] = list(ProductionProgram.objects.values_list('quantity_1', flat=True))
    data_dict[1] = list(ProductionProgram.objects.values_list('quantity_2', flat=True))
    data_dict[2] = list(ProductionProgram.objects.values_list('quantity_3', flat=True))
    data_dict[3] = list(ProductionProgram.objects.values_list('quantity_4', flat=True))
    return pd.DataFrame.from_dict(data=data_dict)


def calculate_overall_consumption() -> dict:
    """
    :return: dictionary with 29 elements: {1: ['110', '140', '150', '150']}
    """
    from apps.production_orders.models import get_df_from_buyparts_production_data
    from apps.sellwish_directsales.models import get_df_from_sellwish_data
    import pandas as pd
    df_1 = get_df_from_buyparts_production_data()
    df_2 = get_df_from_sellwish_data()
    res_df = df_1.dot(df_2)  # matrix multiplication
    res_df.index = res_df.index + 1
    calculation_dict = res_df.to_dict(orient='index')  # {1: {1: 100, 2: 100, 3: 100, 4: 100}, ...}
    output = dict()
    # flatten the calculation_dict structure
    for k, outer_v in calculation_dict.items():
        output[k] = [str(x) if x != "" else str(0) for x in outer_v.values()]
    return output


def get_columns_as_tuples_from_selldirect() -> list:
    """
    returns contents of each row of Directsales table in form of tuple
    in this order: (product_id, quantity, price, penalty)
    :return: [('1', '0', '0.0', '0.0'),...]
    """
    result_list: list = []
    article_ids: list = [str(i[1]) for i in Directsales.objects.values_list("product_id", flat=True)]
    quantity: list = [str(i) for i in Directsales.objects.values_list("quantity", flat=True)]
    prices: list = [str(round(i, 1)) for i in Directsales.objects.values_list("price", flat=True)]
    penalties: list = [str(round(i, 1)) for i in Directsales.objects.values_list("penalty", flat=True)]
    for ar, qu, pr, pe in zip(article_ids, quantity, prices, penalties):
        if qu != "0" or pr != "0.0" or pe != "0.0":
            result_list.append((ar, qu, pr, pe))
    return result_list


def get_article_quantity_tuples_from_sellwish() -> list:
    """
    gets data from user input in userForm on the first page
    in this order: (product_id, quantity)
    :return: f.e. [(1, 100),...]
    """
    _temp = list(Sellwish.objects.values_list("product_id", "quantity"))
    temp = [(x[0][1:], str(x[1])) for x in _temp]  # truncate product_id "P1" -> "1"
    return temp
