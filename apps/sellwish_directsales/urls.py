from django.urls import path
from .import views

urlpatterns = [
    path('', views.index, name='index'),
    path('update_sellsinperiod_form', views.update_sellsinperiod_form, name='update_sellsinperiod_form'),
    path('update_production_program_form', views.update_production_program_form, name='update_production_program_form'),
    path('update_directsales_form', views.update_directsales_form, name='update_directsales_form'),
    path('upload_file', views.upload_file, name='upload_file'),
    path('reset_application', views.reset_application, name='reset_application')
]
