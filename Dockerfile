FROM python:3.6.9-buster

# create app folder
RUN mkdir /app/

# install debug tools
RUN apt update && apt install net-tools -y

# copy files into image
COPY ./ /app/

# set working dir
WORKDIR /app/

# install requirements
RUN /usr/local/bin/pip install -r /app/requirements.txt

# set startup.sh permissions
RUN chmod +x /app/startup.sh

# start app
ENTRYPOINT ["/app/startup.sh" ]