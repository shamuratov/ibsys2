`django2.2` project implementation with a bunch of experimental technologies.

---

## Purpose

This project is used to demonstrate implementation of an ibsys2 student project as a web-application in python with django and further technologies.


## Features

- [`django`](https://docs.djangoproject.com/en/2.2/) as core MTV-Framework for a web-app
- [`djangorestframework`](https://www.django-rest-framework.org/) for RESTFul approach
- [`uwsgi`](https://uwsgi-docs.readthedocs.io/en/latest/Install.html) for production-ready deployment
- [`pdoc`](https://pdoc3.github.io/pdoc/) for documentation
- [`pandas`](https://pandas.pydata.org/pandas-docs/stable/) for data wrangling
- [`bokeh`](https://docs.bokeh.org/en/latest/docs/user_guide.html) for data visualisation
- [`django-crispy-forms`](https://django-crispy-forms.readthedocs.io/en/latest/index.html) for more convenience
- [`django-tables2`](https://django-tables2.readthedocs.io/en/latest/index.html) for more convenience
- [`pytest-django`](https://pytest-django.readthedocs.io/en/latest/index.html) for more convenience while testing
- [`drf-yasg`](https://drf-yasg.readthedocs.io/en/stable/) SWAGGER for more convenience
- [`mypy`](https://mypy.readthedocs.io/en/stable/getting_started.html) for static typing
- [`docker`](https://www.docker.com/) for development, testing, and production
- [`Gitlab CI`](https://about.gitlab.com/gitlab-ci/) with full `build`, `test`, and `deploy` pipeline configured by default

## Installation

Firstly, you will need to have python at least of version 3.6+:
Then, you will need to set a virtual environment installed with python version 3.6+.
From virtual environment run the following command: 

```bash
pip3 install -r requirements.txt
```
