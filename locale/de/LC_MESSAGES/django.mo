��    5      �  G   l      �     �     �     �     �     �     �     �     �     	          '  	   5     ?  #   E     i     n     �     �     �     �     �     �     �     �     �       
        !     /     =     K     a     s     �     �     �     �     �  *   �     �               "     =     X     `     r     y     �     �     �     �  B  �     $	     2	     :	     S	     j	     |	     �	  
   �	     �	     �	     �	     �	     �	  %   �	     
     $
  !   @
     b
     o
  
   �
     �
     �
     �
     �
     �
     �
  
   �
     �
                    3     G     [     v     �     �     �  8   �     �     �  &   �  #   "  #   F     j  
   w     �  %   �      �     �     �  	        4               %                  1   ,            $   2   (   '       !   	   +      -                           #            0           )      
                        *   5   3   .             "                     /              &                 Amount to produce Back Capacity-Requirements Current Period Quantity Current Stock Delete Delivery Deviation Time Delivery Time Directsales Discount Quantity Export to XML Item Name Modus Needed Parts for Production Program Next Orders in Progress Orders in Queue Overtime Parts Description Parts Number Penalty Period 2 Quantity Period 3 Quantity Period 4 Quantity Previous period overtime Price Product ID Production P1 Production P2 Production P3 Production time (new) Production-Orders Production-Program Production-Summary Purchase-Orders Quantity Reset Safety Stock Sellwish, Production-Program & Directsales Setup time (new) Shift Show Production-Summary P1 Show Production-Summary P2 Show Production-Summary P3 Station Total time needed Update Update Capacity-Requirements Update Production-Orders Update Purchase-Orders Update Safety Stock Upload Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Herstellmenge Zurück Kapazitätsanforderungen Aktuelle Periode Menge Aktueller Bestand Löschen Lieferzeitunterschiede Lieferzeit Aktualisiere Direktverkäufe Mengenrabatt Als XML exportieren Produktname Modus Benötigte Teile für Vertriebswunsch Weiter Bestellungen in Bearbeitung Bestellungen in der Warteschlange Überstunden Teilebeschreibung Teilnummer Strafe Periode 2 Menge Periode 3 Menge Periode 4 Menge Überstunden vorherige Periode Preis Produkt ID Produktion P1 Produktion P2 Produktion P3 Produktionszeit (neu) Fertigungsaufträge Produktionsprogramm Produktionszusammenfassung Bestellungen Menge Zurücksetzen Sicherheitsbestand Vertriebswunsch, Produktionsprogramm und Direktverkäufe Rüstzeit (neu) Schicht Produktionszusammenfassung P1 anzeigen Einkaufszusammenfassung P2 anzeigen Einkaufszusammenfassung P3 anzeigen Arbeitsplatz Gesamtzeit Aktualisieren Aktualisiere Kapazitätsanforderungen Aktualisiere Fertigungsaufträge Aktualisiere Bestellungen Aktualisiere Sicherheitsbestand Hochladen 