DROP TABLE production_orders_productorder;
DROP TABLE production_orders_production;
DROP TABLE production_orders_warehouse;
DROP TABLE production_orders_products;
DROP TABLE production_orders_buyparts;
DROP TABLE production_orders_ownparts;
DROP TABLE capacity_requirements_capacityrequirement;
DROP TABLE purchase_orders_purchaseorders;
DROP TABLE sellwish_directsales_sellwish;
DROP TABLE sellwish_directsales_directsales;

CREATE TABLE production_orders_productorder( 
 id int NOT NULL PRIMARY KEY,
 article varchar(255),
 quantity int
);

CREATE TABLE production_orders_production( 
 id int NOT NULL PRIMARY KEY,
 article varchar(255),
 quantity int
);

CREATE TABLE production_orders_warehouse( 
 id int NOT NULL PRIMARY KEY
);

CREATE TABLE production_orders_products( 
 id int NOT NULL PRIMARY KEY,
 warehouse int,
 FOREIGN KEY (warehouse) REFERENCES production_orders_warehouse(id),
 item_name varchar(255),
 quantity int
);


ALTER TABLE production_orders_products ADD FOREIGN KEY (warehouse) REFERENCES production_orders_warehouse(id);

CREATE TABLE production_orders_buyparts( 
 production FOREIGN KEY REFERENCES production_orders_production(id),
 warehouse FOREIGN KEY REFERENCES production_orders_warehouse(id),
 id int NOT NULL PRIMARY KEY,
 item_name varchar(255),
 quantity int,
 price float,
 shipping_cost int,
 discount_quantity int
);

CREATE TABLE production_orders_ownparts( 
 production FOREIGN KEY REFERENCES production_orders_production(id),
 warehouse FOREIGN KEY REFERENCES production_orders_warehouse(id),
 id int NOT NULL PRIMARY KEY,
 item_name varchar(255),
 quantity int,
 price float
);

CREATE TABLE capacity_requirements_capacityrequirement( 
 id int NOT NULL PRIMARY KEY,
 workingstation varchar(255),
 shift int,
 overtime int
);


CREATE TABLE purchase_orders_purchaseorders( 
 id int NOT NULL PRIMARY KEY,
 article int,
 quantity int,
 mode ENUM ('fast', 'normal')
);

CREATE TABLE sellwish_directsales_sellwish( 
 id int NOT NULL PRIMARY KEY,
 product_id int,
 quantity_sw int
);

CREATE TABLE sellwish_directsales_directsales( 
 id int NOT NULL PRIMARY KEY,
 product_id int,
 quantity int,
 price float,
 penalty float
);