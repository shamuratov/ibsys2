import pytest

"""
this is a very basic usage of pytest for a ibsys2 project
"""


def hello_world(name):
    return "Hello {}".format(name)


def test_hello():
    assert hello_world("World!") == "Hello World!"
    assert hello_world(420) == "Hello 420"

def test_failed_hello():
    with pytest.raises(AssertionError):
        assert hello_world(None) == "Hello World!"

    with pytest.raises(AssertionError):
        assert hello_world(420) == "Hello 418"

    with pytest.raises(TypeError):
        assert hello_world() == "Hello World!"


def add_numbers(num1, num2):
    print("Add function started...")
    result = num1 + num2
    print("Numbers added. Returning result...")
    return result


def test_add_numbers():
    assert add_numbers(2, 3) == 5


@pytest.mark.django_db(transaction=True)
def test_spam():
    pass