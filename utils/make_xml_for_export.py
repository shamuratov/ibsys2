from xml.etree.ElementTree import ElementTree, Element, SubElement, tostring
from apps.sellwish_directsales.models import (get_columns_as_tuples_from_selldirect,
                                              get_article_quantity_tuples_from_sellwish)
from apps.purchase_orders.models import get_columns_as_tuples_from_purchase_orders
from apps.production_orders.models import get_dict_item_quantity_from_productorders
from apps.capacity_requirements.models import get_article_quantity_tuples_from_workingplaces


def create_part_sellwish():
    """
    creates structure like: <sellwish><item article="1" quantity="200"/>...</sellwish>
    :return: xml.etree.ElementTree.Element
    """
    sellwish_tuples = get_article_quantity_tuples_from_sellwish()
    top_element = Element("sellwish")
    for pair in sellwish_tuples:
        SubElement(top_element, "item", {"article": str(pair[0]), "quantity": str(pair[1])})
    return top_element


def create_part_selldirect():
    """
    creates structure like: <selldirect><item article="1" quantity="0" price="0.0" penalty="0.0"/>...</selldirect>
    :return: xml.etree.ElementTree.Element
    """
    selldirect_tuples = get_columns_as_tuples_from_selldirect()
    top_element = Element("selldirect")
    for pair in selldirect_tuples:
        SubElement(top_element,
                   "item",
                   {"article": str(pair[0]), "quantity": str(pair[1]), "price": str(pair[2]), "penalty": str(pair[3])})
    return top_element


def create_part_orderlist():
    """
    creates structure like: <orderlist><order article="21" quantity="300" modus="5"/>...</orderlist>
    :return: xml.etree.ElementTree.Element
    """
    orderlist_tuples = get_columns_as_tuples_from_purchase_orders()
    top_element = Element("orderlist")
    for pair in orderlist_tuples:
        SubElement(top_element,
                   "order",
                   {"article": str(pair[0]), "quantity": str(pair[1]), "modus": str(pair[2])})
    return top_element


def create_part_productionlist():
    """
    creates structure like: <productionlist><order article="21" quantity="300"/>...</productionlist>
    :return: xml.etree.ElementTree.Element
    """
    production_dict = get_dict_item_quantity_from_productorders()
    top_element = Element("productionlist")
    for k, v in production_dict.items():
        SubElement(top_element, "production", {"article": str(k[1:]), "quantity": str(v)})
    return top_element


def create_part_workingtimelist():
    """
    creates structure like: <workingtimelist><workingtime article="21" quantity="300" modus="5"/>...</workingtimelist>
    :return: xml.etree.ElementTree.Element
    """
    production_tuples = get_article_quantity_tuples_from_workingplaces()
    top_element = Element("workingtimelist")
    for pair in production_tuples:
        SubElement(top_element,
                   "workingtime",
                   {"station": str(pair[0]), "shift": str(pair[1]), "overtime": str(pair[2])})
    return top_element


def combine_all_xml_parts():
    """
    saves xml in a (given) folder after parsing from databases and forms
    :return:
    """
    root = Element('input')
    # if there will be defects - first child should not be hard coded
    SubElement(root, "qualitycontrol", {"type": "no", "losequantity": "0", "delay": "0"})
    root.append(create_part_sellwish())
    root.append(create_part_selldirect())
    root.append(create_part_orderlist())
    root.append(create_part_productionlist())
    root.append(create_part_workingtimelist())
    return tostring(root, encoding='UTF-8', method='xml')


if __name__ == "__main__":
    combine_all_xml_parts()
