
from pprint import PrettyPrinter
from sys import argv
"""
this file contains functions needed to read and write xml files
readinput(userinputpath)
output_xml(*args, **kwargs, path)
"""


def warehouse_parsing(root) -> dict:
    """
    WAREHOUSESTOCK parsing
    :param root:
    :return:
    """
    all_parts_id = list()
    all_parts_amount = list()
    all_parts_price = list()

    # get from each item: int(id), int(amount), float(price) and make dict
    for article in root.findall("./warehousestock/article"):
        all_parts_id.append(int(article.attrib['id']))
        all_parts_amount.append(int(article.attrib['amount']))
        all_parts_price.append(float(article.attrib['price']))

    warehousestock = {
        'parts_ids': all_parts_id,
        'parts_amount': all_parts_amount,
        'parts_prices': all_parts_price}

    del all_parts_id, all_parts_amount, all_parts_price
    return warehousestock


def inwardstockmovement_parsing(root) -> dict:
    """
    INWARDSTOCKMOVEMENT parsing
    :param root:
    :return:
    """
    buy_item_ids = list()
    buy_item_amount = list()
    buy_item_delivery_time = list()
    buy_item_newprice = list()

    # get from each item: int(id), int(amount), float(time), float(piececosts)
    for article in root.findall("./inwardstockmovement/order"):
        buy_item_ids.append(int(article.attrib['article']))
        buy_item_amount.append(int(article.attrib['amount']))
        buy_item_delivery_time.append(float(article.attrib['time']))
        buy_item_newprice.append(float(article.attrib['piececosts']))

    # find out when arrives the booked article
    # time div by 8h, 60min, 3shifts, 5days AND then add 1 period
    buy_item_delivery_time = [1 + (x / (8 * 60 * 3 * 5)) for x in buy_item_delivery_time]
    inwardstockmovement = {
        'buy_item_ids': buy_item_ids,
        'buy_item_amount': buy_item_amount,
        'buy_item_delivery_time': buy_item_delivery_time,
        'buy_item_newprice': buy_item_newprice}
    return inwardstockmovement


def waitingliststock_parsing(root) -> dict:
    """
    WAITINGLISTSTOCK parsing
    :param root:
    :return:
    """
    missing_parts_id = list()
    wasnt_made = list()
    wasnt_made_amount = list()
    missing_wl_order = list()
    # get from each item: int(id), int(amount), float(price) and make dict
    for article in root.findall("./waitingliststock/missingpart"):
        missing_parts_id.append(int(article.attrib['id']))
        for subitem in article.findall("./waitinglist"):
            wasnt_made_amount.append(int(subitem.attrib['amount']))
            missing_wl_order.append(int(subitem.attrib['order']))
            wasnt_made.append(int(subitem.attrib['item']))

    # sorts by missing_wl_order parallelly
    if len(missing_parts_id) == len(wasnt_made) == len(wasnt_made_amount) == len(missing_wl_order):
        _missing_wl_order, _missing_parts_id, _wasnt_made, _wasnt_made_amount = \
            zip(*sorted(zip(missing_wl_order, missing_parts_id, wasnt_made, wasnt_made_amount)))
        missing_wl_order = list(_missing_wl_order)
        missing_parts_id = list(_missing_parts_id)
        wasnt_made = list(_wasnt_made)
        wasnt_made_amount = list(_wasnt_made_amount)

    waitingliststock = {'missing_wl_order': missing_wl_order,
                        'missing_parts_id': missing_parts_id,
                        'wasnt_made': wasnt_made,
                        'wasnt_made_amount': wasnt_made_amount}
    return waitingliststock


def waitinglistworkstations_parsing(root) -> dict:
    """
    waitinglistworkstations parsing
    :param root:
    :return:
    """
    wl_order = list()
    wl_workplace_id = list()
    wl_amount = list()
    wl_timeneed = list()
    wl_item = list()

    # get from each item: int(id), float(timeneed), int(order),
    # int(amount), int(item) and make dict
    for article in root.findall("./waitinglistworkstations/workplace"):
        if article.attrib['timeneed'] != "0":
            for subitem in article.findall("./waitinglist"):
                wl_workplace_id.append(int(article.attrib['id']))
                wl_timeneed.append(float(subitem.attrib['timeneed']))
                wl_order.append(int(subitem.attrib['order']))
                wl_amount.append(int(subitem.attrib['amount']))
                wl_item.append(int(subitem.attrib['item']))

    # sorts by wl_order parallelly
    _wl_order, _wl_workplace_id, _wl_amount, _wl_timeneed, _wl_item = \
        zip(*sorted(zip(wl_order, wl_workplace_id, wl_amount, wl_timeneed, wl_item)))
    wl_order = list(_wl_order)
    wl_workplace_id = list(_wl_workplace_id)
    wl_amount = list(_wl_amount)
    wl_timeneed = list(_wl_timeneed)
    wl_item = list(_wl_item)

    waitinglistworkstations = {
        'order': wl_order,
        'workplace_id': wl_workplace_id,
        'amount': wl_amount,
        'timeneed': wl_timeneed,
        'item': wl_item}
    return waitinglistworkstations


def ordersinwork_parsing(root) -> dict:
    """
    ORDERSINWORK parsing
    :param root:
    :return:
    """
    order = list()
    workplace_id = list()
    amount = list()
    timeneed = list()
    items = list()
    # get from each item: int(id), int(order), int(amount), float(timeneed) and make dict
    for article in root.findall("./ordersinwork/workplace"):
        workplace_id.append(int(article.attrib['id']))
        order.append(int(article.attrib['order']))
        amount.append(int(article.attrib['amount']))
        timeneed.append(float(article.attrib['timeneed']))
        items.append(int(article.attrib['item']))

    # sorts by order parallelly
    _order, _workplace_id, _amount, _timeneed, _items = \
        zip(*sorted(zip(order, workplace_id, amount, timeneed, items)))
    order = list(_order)
    workplace_id = list(_workplace_id)
    amount = list(_amount)
    timeneed = list(_timeneed)
    items = list(_items)

    ordersinwork = {
        'order': order, 'workplace_id': workplace_id, 'amount': amount, 'timeneed': timeneed, 'item': items
    }
    return ordersinwork


def readinput(tree):
    """
    this function reads xml file provided by a user as an input
    or as a command from CLI
    Usage: python parse_input_xml.py <path>
    :param tree:
    :return:
    """
    try:
        root = tree.getroot()

        pp = PrettyPrinter(indent=2, width=80)
        chapter_names = [child.tag for child in tree.getroot()]

        warehousestock = warehouse_parsing(root)
        # if chapter_names have children elements:
        waitingliststock = waitingliststock_parsing(root)
        waitinglistworkstations = waitinglistworkstations_parsing(root)
        ordersinwork = ordersinwork_parsing(root)
    except NameError:
        print("Could not parse the given xml file")
    finally:
        return warehousestock, waitingliststock, waitinglistworkstations, ordersinwork


if __name__ == "__main__":
    if len(argv) == 2 and argv[1] is not None:
        readinput(argv[1])
    else:
        userdatainput = input("please enter path to the xml file\n")
        readinput(userdatainput)
