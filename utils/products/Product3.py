from utils.products.Product import Product


class Product3(Product):

    def __init__(self):
        super(Product3, self).__init__()
        self.id = 3
        self.parts = {
            "K23": 1,
            "K24": 1,
            "K27": 1,
            "E26": {
                "K44": 2,
                "K47": 1,
                "K48": 2,
            },
            "E31": {
                "K24": 1,
                "K27": 1,
                "E16": {
                    "K24": 1,
                    "K28": 1,
                    "K40": 1,
                    "K41": 1,
                    "K42": 2
                },
                "E17": {
                    "K43": 1,
                    "K44": 1,
                    "K45": 1,
                    "K46": 1
                },
                "E30": {
                    "K24": 2,
                    "K25": 2,
                    "E6": {
                        "K33": 1,
                        "K34": 36,
                        "K35": 2,
                        "K36": 1
                    },
                    "E12": {
                        "K32": 1,
                        "K39": 1
                    },
                    "E29": {
                        "K24": 2,
                        "K25": 2,
                        "E9": {
                            "K33": 1,
                            "K34": 36,
                            "K35": 2,
                            "K37": 1,
                            "K38": 1
                        },
                        "E15": {
                            "K32": 1,
                            "K39": 1
                        },
                        "E20": {
                            "K28": 5,
                            "K32": 1,
                            "K59": 2
                        }
                    }
                }
            }
        }
