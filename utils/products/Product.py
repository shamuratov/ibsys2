class Product:

    def __init__(self):
        self.parts = {}
        self.id = -1

    def get_k_part_count(self, count=1):
        """
        Get K-Part count for given product count-works for "P*" - Parts
        :param count:
        :return:
        """
        # return map
        parts_count = {}

        # recursive method to extract k parts
        def count_sub_parts(parts=self.parts):
            for k, v in parts.items():
                if type(v) is dict:
                    count_sub_parts(v)
                else:
                    if k in parts_count:
                        parts_count[k] += v
                    else:
                        parts_count[k] = v

        # go for count
        for i in range(0, count):
            count_sub_parts()

        # do return
        return parts_count

    def get_e_part_count(self, count=1):
        """
        Get E-Part count for given product count
        :param count:
        :return:
        """
        # return map
        parts_count = {}

        # recursive method to extract e parts
        def count_sub_parts(parts=self.parts):
            for k, v in parts.items():
                if type(v) is dict:
                    if k in parts_count:
                        parts_count[k] += 1
                    else:
                        parts_count[k] = 1
                    count_sub_parts(v)

        # go for count
        for i in range(0, count):
            count_sub_parts()

        # do return
        return parts_count

    def get_k_part_count_for_e_part(self, e_part_id, count=1) -> dict:
        """

        :param e_part_id:
        :param count:
        :return:
        """
        parts_count = {}
        e_part_dict = {}

        def find_e_part(parts=self.parts):
            for k, v in parts.items():
                if k == e_part_id:
                    return v
                if type(v) is dict:
                    r = find_e_part(v)
                    if r is not None:
                        return r
            return None

        e_part_dict = find_e_part()

        def count_sub_parts(parts=self.parts):
            for k, v in parts.items():
                if type(v) is dict:
                    count_sub_parts(v)
                else:
                    if k in parts_count:
                        parts_count[k] += v * count
                    else:
                        parts_count[k] = v * count

        count_sub_parts(e_part_dict)

        return parts_count

    def get_e_part_count_for_e_part(self, e_part_id, count=1) -> dict:
        """
        :param e_part_id:
        :param count:
        :return:
        """
        parts_count = {}

        def find_e_part(parts=self.parts):
            for k, v in parts.items():
                if k == e_part_id:
                    return v
                if type(v) is dict:
                    r = find_e_part(v)
                    if r is not None:
                        return r
            return None

        def count_sub_parts(parts=self.parts):
            for k, v in parts.items():
                if type(v) is dict:
                    parts_count[k] = count
                    count_sub_parts(v)

        e_part_dict = find_e_part()
        count_sub_parts(e_part_dict)
        return parts_count
