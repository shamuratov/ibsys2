from utils.products.Product1 import Product1
from utils.products.Product2 import Product2
from utils.products.Product3 import Product3
from utils.products.helper import cumulate_parts_count

p1 = Product1()
p2 = Product2()
p3 = Product3()

k_parts_p1 = p1.get_k_part_count(count=10)
k_parts_p2 = p2.get_k_part_count(count=100)
k_parts_p3 = p3.get_k_part_count(count=100)
ok = p1.get_k_part_count_for_e_part('')
e_parts_p1 = p1.get_e_part_count(count=10)
e_parts_p2 = p2.get_e_part_count(count=100)
e_parts_p3 = p3.get_e_part_count(count=100)
ok = p1.get_k_part_count_for_e_part('')
k_parts = cumulate_parts_count([k_parts_p1, k_parts_p2, k_parts_p3])
e_parts = cumulate_parts_count([e_parts_p1, e_parts_p2, e_parts_p3])

print(k_parts)
print(e_parts)

safety_count_p1 = 10
safety_count_p2 = 10
safety_count_p3 = 10


def from_sell_wish_to_order():
    p1.warehouse_state.ownparts()
    p1.warehouse_state.buyparts()
    p1.warehouse_state.products()  # [{id:1, quantity:20},{id:2, quantity:20},{id:3, quantity:20}]
    p1.production_state.ownparts()
    p1.production_state.buyparts()

    p1_sell_wish_count_ = p1.get_product_sell_wish()
    p2_sell_wish_count = p2.get_product_sell_wish()
    p3_sell_wish_count = p3.get_product_sell_wish()

    p1_direct_sales_count = p1.get_product_direct_sales()
    p2_direct_sales_count = p2.get_product_direct_sales()
    p3_direct_sales_count = p3.get_product_direct_sales()

    for e in p1.warehouse_state.products():
        print(e.quantity)
