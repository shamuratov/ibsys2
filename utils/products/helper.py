def cumulate_parts_count(parts_list=None) -> dict:
    """
    cumulate parts over all "P*" products
    :param parts_list: list with all P*-Products: f.e. ["k_for_P1", "k_for_P2", "k_for_P3"]
    :return: cumulated_parts_count: dict
    """
    cumulated_parts_count = {}

    if parts_list is None:
        parts_list = []

    for p in parts_list:
        for k, v in p.items():
            if k in cumulated_parts_count:
                cumulated_parts_count[k] += v
            else:
                cumulated_parts_count[k] = v
    return cumulated_parts_count
